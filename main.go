package main

import (
	"Project/cmd/go-workshop/service/channels"
	"Project/part1"
)

func main() {
	//part1Test()
	//server.StartServer()
	channels.DispatchWork()
}

func part1Test() {
	//part1.TestVariables()
	//part1.TestStructures()
	//part1.TestPointers()
	//part1.TestCollections()
	//part1.TestMethods()
	part1.TestChannels()
}
