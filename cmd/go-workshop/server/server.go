package server

import (
	"Project/cmd/go-workshop/server/handlers"
	"github.com/labstack/echo/v4"
)

func StartServer() {
	e := echo.New()
	// hello/Cosmin?lastName=George
	e.GET("/hello/:firstName", handlers.HelloWorld)
	routes(e)

	e.Logger.Fatal(e.Start(":8080"))
}

func routes(e *echo.Echo) {
	e.GET("/hello/:firstName", handlers.HelloWorld)
	emails := e.Group("/emails")
	emails.POST("/", handlers.WelcomeEmail)

}
