package handlers

import (
	hello_world "Project/cmd/go-workshop/service/hello-world"
	"github.com/labstack/echo/v4"
	"net/http"
)

func HelloWorld(ctx echo.Context) error {
	firstName := ctx.Param("firstName")
	lastName := ctx.QueryParam("lastName")
	message, err := hello_world.HelloWorld(firstName, lastName)
	if err != nil {
		return ctx.String(http.StatusBadRequest, err.Error())
	}
	return ctx.String(http.StatusOK, message)
}
