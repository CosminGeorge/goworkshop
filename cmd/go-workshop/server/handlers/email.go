package handlers

import (
	"Project/cmd/go-workshop/model"
	"Project/cmd/go-workshop/service/email"
	"github.com/labstack/echo/v4"
	"net/http"
)

var emailService = email.SenderService{
	WelcomeEmailSender: email.WelcomeEmailSender{},
}

func WelcomeEmail(c echo.Context) error {
	welcomeEmail := model.WelcomeEmail{}
	err := c.Bind(&welcomeEmail)
	if err != nil {
		c.Logger().Error(err)
		return err
	}
	err = emailService.Send(welcomeEmail)
	if err != nil {
		c.Logger().Error(err)
		return err
	}
	return c.String(http.StatusOK, "Sent welcome email to "+welcomeEmail.To)
}
