package hello_world

import (
	"errors"
	"fmt"
)

var testString string

func HelloWorld(firstName string, lastName string) (string, error) {
	if lastName == "" {
		return "", errors.New("Last name is mandatory! ")
	}
	helloString := fmt.Sprintf("Hello %s %s", firstName, lastName)
	return helloString, nil
}
