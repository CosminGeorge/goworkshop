package channels

import (
	"Project/cmd/go-workshop/model"
	"fmt"
	"math/rand"
	"time"
)

const BUFFER_SIZE = 10
const WORKERS = 20
const WORK_SIZE = 1000

func DispatchWork() {
	workChannel := make(chan model.Email, BUFFER_SIZE)
	quitChannel := make(chan bool)

	var workers [20]EmailWorker
	for i := 0; i < WORKERS; i++ {
		emailWorker := EmailWorker{
			ID:            i,
			WorkerChannel: workChannel,
			QuitChannel:   quitChannel,
		}
		emailWorker.Start()
		workers[i] = emailWorker
	}

	//	Simulate work
	go func() {
		for i := 0; i <= WORK_SIZE; i++ {
			workChannel <- model.Email{
				From:    "cosmin@workshop.com",
				To:      fmt.Sprintf("email_%d_@workshop.com", rand.Intn(1000)),
				Subject: "Hello from ASync email worker",
			}
		}
	}()

	time.Sleep(5 * time.Second)

	//	Stop workers

	//Variant 1
	//for i := 0; i < WORKERS; i++ {
	//	quitChannel <- true
	//}

	//Variant 2
	for i := range workers {
		workers[i].Stop()
	}
}
