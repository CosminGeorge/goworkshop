package channels

import (
	"Project/cmd/go-workshop/model"
	"fmt"
	"math/rand"
	"time"
)

type EmailWorker struct {
	ID            int
	WorkerChannel chan model.Email
	QuitChannel   chan bool
}

func (worker *EmailWorker) Start() {
	println("Starting worker: ", worker.ID)

	go func() {
		for {
			select {
			case work := <-worker.WorkerChannel:
				time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
				fmt.Printf("Worker %d: Send email to %s!\n", worker.ID, work.To)
			case <-worker.QuitChannel:
				//	We have been asked to stop
				fmt.Printf("Worker %d stopping\n", worker.ID)
				//do cleanup
				return
			}
		}
	}()
}

func (worker *EmailWorker) Stop() {
	worker.QuitChannel <- true
}
