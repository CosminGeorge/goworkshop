package email

import (
	model2 "Project/cmd/go-workshop/model"
	"errors"
)

type EmailBuilder interface {
	Extract() model2.Email
}

type Sender interface {
	Send(model interface{}) error
}

type SenderService struct {
	WelcomeEmailSender Sender
}

func (sender *SenderService) Send(model interface{}) error {
	switch m := model.(type) {
	case model2.WelcomeEmail:
		err := sender.WelcomeEmailSender.Send(m)
		if err != nil {
			return err
		}
	default:
		return errors.New("Email is not supported! ")
	}
	return nil
}
