package email

import (
	model2 "Project/cmd/go-workshop/model"
	"fmt"
)

type WelcomeEmailSender struct {
}

func (sender WelcomeEmailSender) Send(emailBody interface{}) error {
	welcomeEmail := emailBody.(model2.WelcomeEmail)

	fmt.Printf("Sending Welcome Email from %s to %s with body %s\n", welcomeEmail.From, welcomeEmail.To, welcomeEmail.WelcomeMessage)

	return nil
}
