package email_test

import (
	"Project/cmd/go-workshop/model"
	"Project/cmd/go-workshop/service/email"
	"fmt"
	"testing"
)

type MockedWelcomeEmailService struct {
}

func (service MockedWelcomeEmailService) Send(model interface{}) error {
	fmt.Println("Mocked email sent from test!")
	return nil
}

func TestWelcomeEmail(t *testing.T) {
	service := email.SenderService{
		WelcomeEmailSender: MockedWelcomeEmailService{},
	}
	service.Send(model.WelcomeEmail{})
}
