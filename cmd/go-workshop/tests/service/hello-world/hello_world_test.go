package hello_world_test

import (
	hello_world "Project/cmd/go-workshop/service/hello-world"
	"fmt"
	"testing"
)

func TestHelloWorld(t *testing.T) {
	message, err := hello_world.HelloWorld("Cosmin", "Margarit")
	if err != nil {
		t.Error("Error from service ", err.Error())
	}
	expected := "Hello Cosmin Margarit"
	if message != expected {
		t.Errorf("Output is different. Expected %s but got %s", expected, message)
	}
}

func ExampleHelloWorld() {
	message, _ := hello_world.HelloWorld("Cosmin", "Margarit")
	_, err := hello_world.HelloWorld("Cosmin", "")

	fmt.Println(message)
	fmt.Println(err.Error())
	//Output:
	//Hello Cosmin Margarit
	//Last name is mandatory!
}
