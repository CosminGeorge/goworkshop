package model

type WelcomeEmail struct {
	Email
	WelcomeMessage string
}

func (we *WelcomeEmail) Extract() string {
	return we.WelcomeMessage
}
