package model

type EmailType string

const (
	WELCOME_EMAIL        EmailType = "WelcomeEmail"
	NEWSLETTER_SUBSCRIBE EmailType = "NewsletterSubscribe"
)
