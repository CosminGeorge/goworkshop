package part1

import (
	"fmt"
	"strconv"
)

func TestVariables() {
	//var syntax, zero-value variables
	var int1, int2 int
	int1 = 1
	fmt.Println("Integers: ", int1, int2)

	//Inline declaration
	string1 := "Test"
	fmt.Println("Strings: ", string1)

	//Mixed variable declaration
	var mixed1, mixed2, mixed3 = 1, true, "Mixed"
	fmt.Println("Mixed declaration: ", mixed1, mixed2, mixed3)

	//Default values
	var defaultInt int
	var defaultFloat float64
	var defaultString string
	var defaultBool bool

	fmt.Printf("Defaults int: %d, float: %f, string: %s, bool:%t\n", defaultInt, defaultFloat, defaultString, defaultBool)

	//Type inferrence
	//In order for inferrence to work variables must be initialized
	var inferredInt = 1
	var inferredFloat = 123.456
	fmt.Printf("InferedInt value:%d type: %T\n", inferredInt, inferredInt)
	fmt.Printf("InferedFloat value:%f type: %T\n", inferredFloat, inferredFloat)

	//Conversion
	//In go we don't have casting but conversion
	var anInt8 int8 = 98
	convertedInt64 := int64(anInt8)
	convertedString := strconv.Itoa(int(convertedInt64))
	fmt.Printf("8 bit int: %v, type:%T\n", anInt8, anInt8)
	fmt.Printf("64 bit int: %v, type:%T\n", convertedInt64, convertedInt64)
	fmt.Printf("64 float: %v, type:%T\n", float64(convertedInt64), float64(convertedInt64))
	fmt.Printf("String: %v, type:%T\n", convertedString, convertedString)

	//Constants
	const name = "cosmin"
	const secondName = "George"
	fmt.Println(name, secondName)

	const (
		firstName = "Cosmin"
		lastName  = "Margarit"
	)
	fmt.Println(firstName, lastName)

	//IOTA starts at 0 in a block of constants
	const (
		zero = 1 << iota
		one
		two
		three
	)
	fmt.Println(zero, one, two, three)
}
