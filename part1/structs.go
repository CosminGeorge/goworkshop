package part1

import (
	"fmt"
)

type example struct {
	anInt   int
	aString string
}

func TestStructures() {
	//	Declare a struct variable and set it to the zero value
	var example1 example
	fmt.Println(example1)

	//Create and initialize a struct
	fmt.Println(example{10, "Test"})

	// Named fields in initialization
	fmt.Println(example{aString: "NamedParameter", anInt: 10})

	// Partial initialization. Left out fields are zero-value
	fmt.Println(example{aString: "PartialInitialization"})

	fmt.Println(example1.anInt)

	//	Anonymous structures
	anonymousStruct := struct {
		anInt   int
		aString string
	}{}
	fmt.Println("Anonymous struct: ", anonymousStruct)

	//	Inline initialization
	anonymousStruct2 := struct {
		anInt int
		aBool bool
	}{
		anInt: 23,
		aBool: true,
	}
	fmt.Println("Initialized anonymous struct", anonymousStruct2)

	//	Custom types
	type cosmin struct {
		anInt   int
		aString string
	}

	type george struct {
		anInt   int
		aString string
	}

	var c cosmin
	var g george
	//c = g
	c = cosmin(g)

	fmt.Println(c, g)

	e2 := struct {
		anInt   int
		aString string
	}{anInt: 10, aString: "Test"}
	c = e2
	fmt.Println(c, g)
	//	Why?? g is a named type, e2 is a literal type.

	type CosminInt int
	var customInt CosminInt
	customInt = 9
	fmt.Println(customInt)
	//var aaInt int
	//aaInt = customInt
	//fmt.Println(aaInt)
}
