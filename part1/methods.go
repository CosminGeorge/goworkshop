package part1

import (
	"errors"
	"fmt"
	"log"
)

type car struct {
	make  string
	model string
}

var availableColors = map[string]bool{"red": true, "blue": true, "black": false}

func (aCar car) carMethod() {
	aCar.model = "Berlina"
	fmt.Println("This is a car: ", aCar)
}
func (aCar *car) carMethodPointer() {
	aCar.model = "Solenza"
	fmt.Println("This is a car", aCar)
}

func (aCar *car) paintCar(newColor string) (string, error) {
	_, isAvailable := availableColors[newColor]

	if !isAvailable {
		return "", errors.New("Color is not available ")
	}
	return "Painted the car: " + newColor, nil
}

func TestMethods() {
	carr := car{
		make:  "Dacia",
		model: "1300",
	}
	carr.carMethod()
	fmt.Println("After value method: ", carr)
	carr.carMethodPointer()
	fmt.Println("After pointer method: ", carr)

	fmt.Printf("\n\n\n\n\n\n\n\n\n")
	//Methods are display sugar
	//NOTICE: NEVER CALL LIKE THIS
	car.carMethod(carr)
	(*car).carMethodPointer(&carr)
	(*car).paintCar(&carr, "red")
	fmt.Printf("\n\n\n\n\n\n\n\n\n")

	message, err := carr.paintCar("Green")
	if err != nil {
		println("Error while trying to paint the car")
		//No stack-trace
		//log.Fatal(err)
		//Stack-Trace
		log.Panic(err)
		//Unreachable code
		println("\n\n TEST ")
	}
	//	Happy case
	println(message)
}
