package part1

func TestPointers() {
	//Declaration
	//var intPointer *int
	//int :=8;
	//intPointerV2 := &int

	//Pass-by-Value
	counter := 1
	println("Value of counter:", counter, ", Address of counter:", &counter)
	increment(counter)
	println("Value of counter:", counter, ", Address of counter:", &counter)
	println()
	/*

	 */
	//	Sharing Data, Pointer Semantics
	//Side-effects:
	//Mutation outside of scope
	sharedCounter := 1
	println("Value of counter:", sharedCounter, ", Address of counter:", &sharedCounter)
	incrementPointer(&sharedCounter)
	println("Value of counter:", sharedCounter, ", Address of counter:", &sharedCounter)

	/*

		Escape Semantics
	*/
	println()
	createUserValue()
	userPointer := createUserPointer()

	println(userPointer)
}

func increment(counter int) {
	counter++
	println("Value of incremented counter:", counter, ", Address of incremented counter:", &counter)
}
func incrementPointer(pointer *int) {
	(*pointer)++ // == sharedCounter
	println("Value of incremented counter:", *pointer, ", Address of incremented counter:", pointer)
	println("Address of pointer in frame ", &pointer)

}

type user struct {
	name  string
	email string
}

//Value semantics
//Caller gets their own copy of a user
//Very fast because the memory is already allocated
//Isolation, immutability
//Self-cleaning on the way down
func createUserValue() user {
	u := user{
		name:  "Cosmin",
		email: "cosmin@email.com",
	}
	println("Value user: ", &u)

	return u
}

//Pointer semantics
//Escaped Analysis
func createUserPointer() *user {
	u := user{
		name:  "Cosmin",
		email: "cosmin@email.com",
	}
	println("Pointer user:", &u)

	return &u
}
