package part1

import (
	"fmt"
	"math/rand"
	"time"
)

func TestChannels() {
	//waitForTask()
	//waitForResult()
	//fanOut()
	//drop()
	waitForTasks()
}

func defineChannels() {
	// nil channel
	var ch chan string

	//assign to nil
	ch = nil

	//	open channel
	ch = make(chan string)

	//closed
	close(ch)
}

//Signal With Data- Guarantee- Unbuffered
func waitForTask() {
	//Think about being a manager and hiring a new employee.
	//In this scenario, you want your new employee to perform a task but they need to wait until you are ready.
	//This is because you need to hand them a piece of paper before they start.
	ch := make(chan string)
	start := time.Now()

	go func() {
		p := <-ch
		fmt.Println("Channel received message: ", p)
		fmt.Println("After: ", time.Now().Sub(start))
	}()

	time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
	ch <- "paper"

	//Don't kill main thread just yet
	time.Sleep(1 * time.Second)
}

func waitForResult() {
	//In this next scenario things are reversed.
	//This time you want your new employee to perform a task immediately when they are hired,
	//and you need to wait for the result of their work.
	//You need to wait because you need the paper from them before you can continue.
	ch := make(chan string)
	start := time.Now()

	go func() {
		time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
		ch <- "paper"
	}()

	p := <-ch
	fmt.Println("Channel received message: ", p)
	fmt.Println("After: ", time.Now().Sub(start))

	//Don't kill main thread just yet
	time.Sleep(1 * time.Second)
}

//Signal With Data- No Guarantee- Buffered >1

func fanOut() {
	//Imagine you are the manager again but this time you hire a team of employees.
	//You have an individual task you want each employee to perform.
	//As each individual employee finishes their task,
	//they need to provide you with a paper report that must be placed in your BOX on your desk.
	emps := 20
	ch := make(chan string, emps)

	for i := 0; i < emps; i++ {
		go func(employee int) {
			time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
			ch <- fmt.Sprintf("Paper from employee %d", employee)
		}(i)
	}

	for emps > 0 {
		p := <-ch
		fmt.Println(p)
		emps--
	}
}

func drop() {
	//Imagine you are the manager again and you hire a single employee to get work done.
	//You have an individual task you want the employee to perform.
	//As the employee finishes their task you don’t care to know they are done.
	//All that’s important is whether you can or can’t place new work in the box.
	//If you can’t perform the send, then you know your box is full and the employee is at capacity.
	//At this point the new work needs to be discarded so things can keep moving.
	const cap = 5
	ch := make(chan string, cap)

	go func() {
		for p := range ch {
			fmt.Println("employee received: ", p)
		}
	}()

	const work = 20

	for i := 0; i < work; i++ {
		select {
		case ch <- "paper":
			fmt.Println("manager send work")
		default:
			fmt.Println("manager DROPPED message")
		}
	}
}

//Delayed Guarantee, Buffered = 1
func waitForTasks() {
	ch := make(chan string, 1)

	go func() {
		for p := range ch {
			fmt.Println("employee received: ", p)
		}
	}()

	const work = 20
	for i := 0; i < work; i++ {
		time.Sleep(time.Duration(rand.Intn(500)) * time.Millisecond)
		ch <- "paper"
	}
}
