package part1

import "fmt"

func TestCollections() {
	testArrays()
	testSlices()
	testMaps()
}

func testArrays() {
	fmt.Println("Arrays:")
	//	Declaration
	const size = 5
	var anArray [size]int
	anArray[0] = 1
	anArray[1] = 2
	anArray[2] = 3
	//anArray[10] = 10
	fmt.Println(anArray)

	fmt.Print("Classic for: ")
	for i := 0; i < size; i++ {
		fmt.Printf(" i:%d, el:%d |", i, anArray[i])
	}
	println()

	fmt.Print("Go Range: ")
	for i, el := range anArray {
		fmt.Printf(" i:%d, el:%d, address:%d |", i, el, &anArray[i])
	}
	println()

	fmt.Print("Go Range Pointer Semantic: ")
	for i := range anArray {
		fmt.Printf(" el:%d|", anArray[i])
	}
	println()

	//Test Value and Pointer

	testArray := [5]string{"Ana", "Maria", "Bogdan", "Edi", "Vasile"}

	fmt.Printf("\nPointer semantic: Before:  testArray[1]=%s, ", testArray[1])
	for i := range testArray {
		testArray[1] = "Alexandra"

		if i == 1 {
			fmt.Printf("After: testArray[1]=%s", testArray[1])
		}
	}

	testArray2 := [5]string{"Ana", "Maria", "Bogdan", "Edi", "Vasile"}
	fmt.Printf("\nValue semantic: Before:  testArray[1]=%s, ", testArray2[1])
	for i, v := range testArray2 {
		testArray2[1] = "Alexandra"

		if i == 1 {
			fmt.Printf("After: testArray[1]=%s", v)
		}
	}

}

func testSlices() {
	fmt.Printf("\n\nSlices:\n")

	//	Declaration
	makeInts := make([]int, 3, 6)
	makeIntsJustLen := make([]int, 3)
	var nilSlice []int
	emptySlice := []int{}
	makeInts[0] = 1
	makeInts[1] = 2
	//makeInts[4]=2 OoR

	fmt.Printf("Len:%d, Cap:%d, Slice:%v\n", len(makeInts), cap(makeInts), makeInts)
	fmt.Printf("Len:%d, Cap:%d, Slice:%v\n", len(makeIntsJustLen), cap(makeIntsJustLen), makeIntsJustLen)
	fmt.Printf("Len:%d, Cap:%d, Slice:%v\n", len(nilSlice), cap(nilSlice), nilSlice)
	if nilSlice == nil {
		println(" NilSlice Slice is nil")

		for i := 0; i < 10; i++ {

		}
	}
	if emptySlice != nil && len(emptySlice) == 0 {
		println(" EmptySlice Slice is empty, not nil")

		for i := 0; i < 10; i++ {
			nilSlice = append(nilSlice, i)
		}
		fmt.Printf("Len:%d, Cap:%d, Slice:%v\n", len(nilSlice), cap(nilSlice), nilSlice)
	}
}

func testMaps() {
	fmt.Printf("\n\nMaps:\n")
	var zeroMap map[int]string
	makeMap := make(map[int]string)

	makeMap[1] = "Unu"
	makeMap[2] = "Doi"
	makeMap[3] = "Trei"

	fmt.Println("ZeroMap: ", zeroMap)
	fmt.Println("MakeMap: ", makeMap)

	for key, value := range makeMap {
		fmt.Printf(" Key:%d Value:%s |", key, value)
	}

	four, ok := makeMap[4]
	fmt.Println("Four ", four, "Exists: ", ok)

	delete(makeMap, 3)
	fmt.Println(makeMap)
}
